/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author micebishop
 */
public class Book {
    private String title, edition, isbn;
    private Author[] listOfAuthors;
    private int copies;
    
    public Book(String title, String edition, String isbn, Author[] listOfAuthors, int copies){
        this.title = title;
        this.edition = edition;
        this.isbn = isbn;
        this.listOfAuthors = listOfAuthors;
        this.copies = copies;        
    }
    
    
    
    
    
    
    

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the edition
     */
    public String getEdition() {
        return edition;
    }

    /**
     * @param edition the edition to set
     */
    public void setEdition(String edition) {
        this.edition = edition;
    }

    /**
     * @return the isbn
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * @param isbn the isbn to set
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     * @return the author
     */
    public Author[] getListOfAuthors() {
        return listOfAuthors;
    }

    /**
     * @param author the author to set
     */
    public void setListOfAuthors(Author[] listOfAuthors) {
        this.listOfAuthors = listOfAuthors;
    }

    /**
     * @return the copies
     */
    public int getCopies() {
        return copies;
    }

    /**
     * @param copies the copies to set
     */
    public void setCopies(int copies) {
        this.copies = copies;
    }
    
    
}
