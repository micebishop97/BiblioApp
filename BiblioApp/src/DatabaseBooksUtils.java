
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.DefaultListModel;

/**
 *
 * @author micebishop
 */
public class DatabaseBooksUtils {
    private Connection conn;
    
    public DatabaseBooksUtils(){
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Driver O.K.");
            
            String url = "jdbc:postgresql://localhost/bappdb";
            String user = "micebishop";
            String passwd = "messi10";
            if (this.conn == null) {
                this.conn = DriverManager.getConnection(url, user, passwd);
                System.out.print("Connection Effective !");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }  
    }
    
    public void createAuthor(Author author){
        try {
            Statement crauthorState = conn.createStatement();
            ResultSet crauthorResult = crauthorState.executeQuery("INSERT INTO author (firstName, lastName) VALUES('"
                    + author.firstName +"','"                
                    + author.lastName +"') RETURNING id");
             if (crauthorResult.next()){
                System.out.print("\n Id: "+crauthorResult.getString("id")+"\n");
                author.setId(crauthorResult.getInt("id"));
            }
                    
                    } catch (SQLException ex) {
                        System.err.println(ex.toString());
        } 
        
    }
    public void createBookAuthor(String isbn, int idAuteur){
        try {
            Statement crBaState = conn.createStatement();
            ResultSet crBaResult = crBaState.executeQuery("INSERT INTO book_author (isbn, idauthor) VALUES('"
                    + isbn +"','"                
                    + idAuteur +"') RETURNING idauthor");
                    
                    } catch (SQLException ex) {
                        System.err.println(ex.toString());
        } 
        
    }
    
    public void createBook(Book book){
        try {
            System.out.println("Bonjour je suis laaaaaaaaaa");
            Statement crbookState = conn.createStatement();
            ResultSet crbookResult = crbookState.executeQuery("INSERT INTO book (title, edition, isbn, copies) VALUES('"
                    + book.getTitle() + "','"
                    + book.getEdition()+ "','"
                    + book.getIsbn()+ "','"
                    + book.getCopies()+ "') RETURNING isbn");
            System.out.println("Taille tableau: "+book.getListOfAuthors().length);
            for (int i=0; i<book.getListOfAuthors().length; i++){
                createBookAuthor(book.getIsbn(), book.getListOfAuthors()[i].getId());
                
            }
            
                    
                    
                    } catch (SQLException ex) {
                        System.err.println(ex.toString());
        } 
    }
    
    public int countBook(){
        int nbre = 0;
        try {
            Statement countBookState = conn.createStatement();
            ResultSet countBookResultSet = countBookState.executeQuery("SELECT COUNT(*) FROM book");
            countBookResultSet.next();
            nbre = countBookResultSet.getInt("count");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseBooksUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return nbre;
    }
    
    public int countAuthor(){
        int nbre = 0;
        try {
            Statement countAuthorState = conn.createStatement();
            ResultSet countAuthorResultSet = countAuthorState.executeQuery("SELECT COUNT(*) FROM author");
            countAuthorResultSet.next();
            nbre = countAuthorResultSet.getInt("count");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseBooksUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return nbre;
    }
    
    public DefaultListModel addListAuthor(){
        try {
            DefaultListModel listOfAuthors = new DefaultListModel();
            Statement listFnAuthorState = conn.createStatement();
            ResultSet listFnAuthorResultSet = listFnAuthorState.executeQuery("SELECT firstName FROM author");
            Statement listLnAuthorState = conn.createStatement();
            ResultSet listLnAuthorResultSet = listLnAuthorState.executeQuery("SELECT lastName FROM author");
            while(listFnAuthorResultSet.next() && listLnAuthorResultSet.next()){
                
                String all = listFnAuthorResultSet.getString("firstName")+listLnAuthorResultSet.getString("lastName");
                System.out.println(all);
                listOfAuthors.addElement(all);
            }
         return listOfAuthors;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseBooksUtils.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
