
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author micebishop
 */
public class DatabaseLibrarianUtils {
    
    private static Connection conn;

    /**
     * @return the conn
     */
    public static Connection getConn() {
        return conn;
    }

    /**
     * @param aConn the conn to set
     */
    public static void setConn(Connection aConn) {
        conn = aConn;
    }
    // Constructeurs
    public DatabaseLibrarianUtils(){
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Driver O.K.");
            
            String url = "jdbc:postgresql://localhost/bappdb";
            String user = "micebishop";
            String passwd = "messi10";
            if (this.conn == null) {
                this.conn = DriverManager.getConnection(url, user, passwd);
                System.out.print("Connection Effective !");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }       
        
    }

    // Méthodes for Class Librarian ==========================================================================================
    public void createLibrarian(Librarian librarian) {
        try {
            Statement crlibState = conn.createStatement(); 
            ResultSet crlibResult = crlibState.executeQuery("INSERT INTO librarian (firstName, lastName, nickname, password) VALUES('"
                    + librarian.getFirstName() + "','"
                    + librarian.getLastName() + "','"
                    + librarian.getNickname() + "','"
                    + librarian.getPassword() + "')");
                    
                    
                    } catch (SQLException ex) {
                        System.err.println(ex.toString());
        }     
        
    }
    
    
    public boolean nicknameExists(String nickname){
        boolean existe = false;
        try {
            Statement grepnickState = getConn().createStatement(); 
            ResultSet grepnickResult = grepnickState.executeQuery("SELECT nickname FROM librarian WHERE nickname='"+nickname+"'");
            ResultSetMetaData grepnickMeta = grepnickResult.getMetaData();
            if (grepnickResult.next()){
                System.out.print("\n Nickname: "+grepnickResult.getString("nickname")+"\n");
                if (grepnickResult.getString("nickname").equals(nickname)){
                    existe = true;                  
                    
                }
            }
            /*while (grepnickResult.next()){
                System.out.print("Je suis dans le while");
               
                for(int i = 1; i <= grepnickMeta.getColumnCount(); i++)
                    System.out.print("\t" + grepnickResult.getObject(i).toString() + "\t|");
            }*/                    
            } catch (SQLException ex) {
                        System.err.println(ex.toString());
        }
        return existe;
    }
    
    public int getIdFromDb(String nickname){
        int id=0;
        try {
            Statement grepidState = conn.createStatement(); 
            ResultSet grepidResult = grepidState.executeQuery("SELECT id FROM librarian WHERE nickname='"+nickname+"'");
            if (grepidResult.next()){
                System.out.print("\n Id: "+grepidResult.getString("id")+"\n");
                id = grepidResult.getInt("id");
            }
            /*while (grepnickResult.next()){
                System.out.print("Je suis dans le while");
               
                for(int i = 1; i <= grepnickMeta.getColumnCount(); i++)
                    System.out.print("\t" + grepnickResult.getObject(i).toString() + "\t|");
            }*/                    
            } catch (SQLException ex) {
                        System.err.println(ex.toString());
        }
        return id;       
    }
    
    public boolean pwIsCorrect(String nickname, String password){
        boolean correct = false;
        try {
            Statement greppwState = conn.createStatement(); 
            ResultSet greppwResult = greppwState.executeQuery("SELECT password FROM librarian WHERE nickname='"+nickname+"'");
            greppwResult.next();
            System.out.print("\n Password: "+greppwResult.getString("password")+"\n");
            if (greppwResult.getString("password").equals(password)){
                correct = true;                
            }                   
            } catch (SQLException ex) {
                        System.err.println(ex.toString());
        } 
        return correct;
        
    }
    
    //End of  Méthodes for Class Librarian ==========================================================================================
    
    
    
}
