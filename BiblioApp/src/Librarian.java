/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author micebishop
 */
public class Librarian extends Person{
    private String nickname, password;
    
    public String getPassword(){
        return password;
    }
    
    public void setPassword(String password){
        this.password = password;
    }
    
    //
    public String getNickname(){
        return nickname;
    }
    
    public void setNickname(String nickname){
        this.nickname = nickname;
    }
    
    // Constructeurs
    public Librarian(String firstName, String lastName, String nickname, String password){
        super(firstName, lastName);
        this.nickname = nickname;
        this.password = password;
    }
    
}
